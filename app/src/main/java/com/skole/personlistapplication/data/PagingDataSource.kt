package com.skole.personlistapplication.data

import androidx.paging.PagingSource
import androidx.paging.PagingState
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

/**
 * Created by Selahattin Kole on 29.08.2021.
 */
class PagingDataSource(private val dataSource: DataSource): PagingSource<String, Person>() {
    override suspend fun load(params: LoadParams<String>): LoadResult<String, Person> {
        return try {
            getPersonList(params.key)
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    private suspend fun getPersonList(next:String? = null) = suspendCoroutine<LoadResult<String, Person>> { coroutine ->
        dataSource.fetch(next, object : FetchCompletionHandler {
            override fun invoke(response: FetchResponse?, error: FetchError?) {
                coroutine.resume(when{
                        response != null -> LoadResult.Page(
                        data = response.people,
                        prevKey = next,
                        nextKey = response.next.orEmpty()
                    )
                    else -> LoadResult.Error(Throwable(message = error?.errorDescription))
                })
            }
        })
    }

    override fun getRefreshKey(state: PagingState<String, Person>): String? {
        return null
    }
}