package com.skole.personlistapplication.base.application

import android.app.Application
import com.skole.personlistapplication.base.di.dataSourceModule
import com.skole.personlistapplication.base.di.repositoryModule
import com.skole.personlistapplication.base.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Created by Selahattin Kole on 29.08.2021.
 */
class PersonListApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@PersonListApplication)
            modules(listOf(viewModelModule, repositoryModule, dataSourceModule))
        }
    }
}