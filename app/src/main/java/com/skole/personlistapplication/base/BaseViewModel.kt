package com.skole.personlistapplication.base

import androidx.lifecycle.ViewModel

/**
 * Created by Selahattin Kole on 28.08.2021.
 */
open class BaseViewModel: ViewModel(){
}