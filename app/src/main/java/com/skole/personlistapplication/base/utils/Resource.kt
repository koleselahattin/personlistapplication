package com.skole.personlistapplication.base.utils

import com.skole.personlistapplication.data.FetchError
import com.skole.personlistapplication.data.FetchResponse

/**
 * Created by Selahattin Kole on 29.08.2021.
 */
sealed class Resource {
    data class PersonListSuccess(val data: FetchResponse?) : Resource()
    data class Error(val error: FetchError?) : Resource()
    object Loading : Resource()
}