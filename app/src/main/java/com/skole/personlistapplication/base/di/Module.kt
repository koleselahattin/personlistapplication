package com.skole.personlistapplication.base.di

import com.skole.personlistapplication.data.DataSource
import com.skole.personlistapplication.data.PagingDataSource
import com.skole.personlistapplication.personlist.PersonListRepository
import com.skole.personlistapplication.personlist.PersonListViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Selahattin Kole on 29.08.2021.
 */

val viewModelModule = module {
    viewModel {
        PersonListViewModel()
    }
}

val repositoryModule = module {
    single {
        PersonListRepository(get())
    }
}

val dataSourceModule = module {
    single {
        DataSource()
    }
    single {
        PagingDataSource(get())
    }
}