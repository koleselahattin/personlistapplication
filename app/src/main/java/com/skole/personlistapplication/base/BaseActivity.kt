package com.skole.personlistapplication.base

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding

/**
 * Created by Selahattin Kole on 28.08.2021.
 */
abstract class BaseActivity<VM: BaseViewModel>: ComponentActivity() {
}