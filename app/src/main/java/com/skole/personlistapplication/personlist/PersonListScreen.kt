package com.skole.personlistapplication.personlist

import android.os.Bundle
import android.util.Log
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.paging.LoadState
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.items
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.skole.personlistapplication.base.BaseActivity
import com.skole.personlistapplication.data.Person
import com.skole.personlistapplication.personlist.ui.theme.PersonListApplicationTheme
import org.koin.android.viewmodel.ext.android.viewModel

class PersonListScreen : BaseActivity<PersonListViewModel>() {

    private val viewModel by viewModel<PersonListViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            PersonListApplicationTheme {
                Surface(color = MaterialTheme.colors.background) {
                    PersonListScreenContent(resource = viewModel.personList.collectAsLazyPagingItems())
                }
            }
        }
    }

    @Composable
    private fun PersonListScreenContent(resource: LazyPagingItems<Person>) {
        PersonList(personList = resource)
    }

    @Composable
    fun PersonList(personList: LazyPagingItems<Person>) {
        val refreshing = rememberSwipeRefreshState(isRefreshing = false)
        SwipeRefresh(
            state = refreshing,
            onRefresh = { personList.refresh() }, modifier = Modifier
                .fillMaxWidth()
                .wrapContentWidth(
                    Alignment.CenterHorizontally
                )
        ) {
            refreshing.isRefreshing = personList.loadState.refresh is LoadState.Loading
            LazyColumn(modifier = Modifier.fillMaxWidth()) {
                items(personList) { item ->
                    item?.let {
                        PersonItem(it)
                    }
                }
                when {
                    personList.loadState.refresh is LoadState.Error -> {
                        personList.retry()
                        Log.e("refreshError","${(personList.loadState.refresh as LoadState.Error).error.localizedMessage.orEmpty()}")
                    }
                    personList.loadState.append is LoadState.Error -> {
                        personList.retry()
                        Log.e("appendError","${(personList.loadState.append as LoadState.Error).error.localizedMessage.orEmpty()}")
                    }
                    personList.loadState.refresh is LoadState.Loading -> {
                        item {
                            Column(
                                modifier = Modifier.fillParentMaxSize(),
                                verticalArrangement = Arrangement.Center,
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {
                                CircularProgressIndicator()
                            }
                        }
                    }
                    personList.loadState.append is LoadState.Loading -> {
                        item {
                            CircularProgressIndicator(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .wrapContentWidth(Alignment.CenterHorizontally)
                            )
                        }
                    }
                }
            }
        }
    }

    @Composable
    fun PersonItem(person: Person) {
        Text(text = "${person.fullName} (${person.id})", modifier = Modifier.padding(16.dp))
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    PersonListApplicationTheme {
    }
}