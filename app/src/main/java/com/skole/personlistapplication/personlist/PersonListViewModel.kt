package com.skole.personlistapplication.personlist

import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.skole.personlistapplication.base.BaseViewModel
import com.skole.personlistapplication.data.DataSource
import com.skole.personlistapplication.data.PagingDataSource
import com.skole.personlistapplication.data.Person
import kotlinx.coroutines.flow.Flow

/**
 * Created by Selahattin Kole on 28.08.2021.
 */
class PersonListViewModel :
    BaseViewModel() {

    var personList: Flow<PagingData<Person>> =
        Pager(PagingConfig(20)) { PagingDataSource(DataSource()) }.flow.cachedIn(viewModelScope)
}